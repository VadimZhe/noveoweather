from datetime import datetime

from django.utils.timezone import make_aware


def openweather_processor(resp_json):
    ts = resp_json.get('dt', 0)
    return {
        "location": {
            "lon": resp_json.get('coord', {}).get('lon', None),
            "lat": resp_json.get('coord', {}).get('lat', None),
        },
        "title": resp_json.get('name', None),
        "timestamp": make_aware(
            datetime.utcfromtimestamp(ts)).isoformat() if ts else None,
        "temperature": {
            "C": resp_json.get('main', {}).get('temp', None),
        },
        "humidity": {
            "percent": resp_json.get('main', {}).get('humidity', None),
        },
        "pressure": {
            "atm": resp_json.get('main', {}).get('pressure', None),
        },
    }


def yandexpogoda_processor(resp_json):
    raise NotImplementedError
