from cache_memoize import cache_memoize
from requests import get

from django.conf import settings
from django.http import JsonResponse
from django.utils.module_loading import import_string
from django.views import View


@cache_memoize(settings.CACHE_TTL)
def get_request(url):
    return get(url)


class WeatherView(View):
    def get(self, request, *args, **kwargs):
        location = request.GET.get(
            'location', settings.DEFAULT_WEATHER_LOCATION)
        source_name = request.GET.get(
            'source', settings.DEFAULT_WEATHER_SOURCE)
        source_data = settings.WEATHER_SOURCES.get(source_name, None)
        if not source_data:
            return JsonResponse({'message': 'unknown source'}, status=400)
        processor = import_string(
            f"{settings.WEATHER_PROCESSORS_MODULE}.{source_data['processor']}")
        response = get_request(source_data['url'].format(location=location))
        if response.status_code == 200:
            return JsonResponse(processor(response.json()))
        return JsonResponse({
            'raw_reply': response.text,
            'message': 'Failed to get weather info, see raw_reply',
        })
