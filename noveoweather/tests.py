import json

from django.conf import settings
from django.test import TestCase, Client, override_settings

from unittest.mock import patch

from .views import get_request


def invalidate_cache(location):
    get_request.invalidate(
        settings.WEATHER_SOURCES[
            settings.DEFAULT_WEATHER_SOURCE]['url'].format(location=location))


class MockResponse:
    def __init__(self, status=200, body={}):
        self.body = body
        self.status_code = status
        self.text = json.dumps(body)

    def json(self):
        return self.body


@patch('noveoweather.views.get', return_value=MockResponse())
@override_settings(CACHE_TTL=0)
class WeatherTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.mock_response = MockResponse(body={
            'coord': {'lon': 1, 'lat': 2},
            'name': 'Testbridge',
            'dt': 3,
            'main': {'temp': 4, 'humidity': 5, 'pressure': 6},
        })

    def test_weather_requested(self, mock_get):
        mock_get.return_value = self.mock_response
        invalidate_cache('Testbridge')
        response = self.client.get(
            '/weather/current/', {'location': 'Testbridge'})
        self.assertEqual(response.status_code, 200)
        resp_json = response.json()
        self.assertIn('title', resp_json)
        self.assertEqual(resp_json['title'], 'Testbridge')
        self.assertTrue(mock_get.called)
        self.assertIn(
            settings.WEATHER_SOURCES[
                settings.DEFAULT_WEATHER_SOURCE]['url'][:20],
            mock_get.call_args[0][0])
        self.assertIn('Testbridge', mock_get.call_args[0][0])

    def test_no_location_specified(self, mock_get):
        mock_get.return_value = self.mock_response
        response = self.client.get('/weather/current/')
        self.assertEqual(response.status_code, 200)
        resp_json = response.json()
        self.assertIn('title', resp_json)
        self.assertEqual(resp_json['title'], 'Testbridge')
        self.assertTrue(mock_get.called)

    def test_unknown_source_specified(self, mock_get):
        response = self.client.get(
            '/weather/current/',
            {'location': 'London', 'source': 'somestrangesource'})
        self.assertEqual(response.status_code, 400)
        resp_json = response.json()
        self.assertIn('message', resp_json)
        self.assertEqual(resp_json['message'], 'unknown source')
        self.assertFalse(mock_get.called)

    def test_bad_response(self, mock_get):
        mock_get.return_value = MockResponse(
            status=401, body={'cod': 401, 'message': "You were totally wrong"})
        invalidate_cache('London')
        response = self.client.get(
            '/weather/current/', {'location': 'London'})
        self.assertEqual(response.status_code, 200)
        resp_json = response.json()
        self.assertIn('message', resp_json)
        self.assertEqual(
            resp_json['message'],
            'Failed to get weather info, see raw_reply')
        self.assertTrue(mock_get.called)
        self.assertIn('raw_reply', resp_json)
        self.assertIn('You were totally wrong', resp_json['raw_reply'])


@patch('noveoweather.views.get', return_value=MockResponse())
class CacheTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_requests_cached(self, mock_get):
        self.client.get('/weather/current/', {'location': 'London'})
        self.assertEqual(mock_get.call_count, 1)
        self.client.get('/weather/current/', {'location': 'London'})
        self.assertEqual(mock_get.call_count, 1)
        invalidate_cache('London')
        self.client.get('/weather/current/', {'location': 'London'})
        self.assertEqual(mock_get.call_count, 2)
